function graficarMapa(){
    var mymap = L.map('mapid').setView([4.570810232930141, -74.11494970321657], 17);
    
    
    L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
        maxZoom: 18,
        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
            '<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
            'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
        id: 'mapbox/streets-v11',
        tileSize: 512,
        zoomOffset: -1
    }).addTo(mymap);
     
    var marker = L.marker([4.571706, -74.115273]).addTo(mymap);
    var marker1 = L.marker([4.571604, -74.115522]).addTo(mymap);
    var marker2 = L.marker([4.571199, -74.115387]).addTo(mymap);
    var marker3 = L.marker([4.570204, -74.115681]).addTo(mymap);
    
    marker.bindPopup("<b>Iglesia Granjas de San Pablo</b>").openPopup();
    marker1.bindPopup("<b>Salón Comunal Granjas de San Pablo</b>").openPopup();
    marker2.bindPopup("<b>Parque Granjas de San Pablo</b>").openPopup();
    marker3.bindPopup("<b>Parque</b>").openPopup();
    
    var polygon = L.polygon([          
      [
        4.571869008848348,
        -74.11486387252808
        
      ],
      [
        4.571778103906664,
        -74.11527156829834
      
      ],
      [
        4.5716604622003,
        -74.11589920520782
        
      ],
      [
        4.5716016413398854,
        -74.11615133285521
        
      ],
      [
        4.570879748568707,
        -74.11600649356842
        
      ],
      [
        4.5704626546361435,
        -74.11589920520782
        
      ],
      [
        4.570248760217601,
        -74.11588847637177
        
      ],
      [
        4.569943960560884,
        -74.1155344247818
        
      ],
      [
        4.569714023891993,
        -74.11576509475708
        
      ],
      [
        4.5695910344806805,
        -74.11530375480652
        
      ],
      [
        4.569858402739206,
        -74.11496043205261
        
      ],
      [
        4.569949307924407,
        -74.11431133747101
        
      ],
      [
        4.570361054795294,
        -74.11324381828308
        
      ],
      [
        4.571366357849909,
        -74.1140967607498
        
      ],
      [
        4.571885050895688,
        -74.11415040493011
        
      ],
      [
        4.572002692565169,
        -74.11435961723326
        
      ],
      [
        4.571869008848348,
        -74.11486387252808
        
      ]],{
        
color: 'black',
fillColor: '#E14DE8',
fillOpacity: 0.3,

}).addTo(mymap);

function clickIglesia(e){
    console.log('iglesia');

    document.getElementById("iglesia").style.visibility = "visible";
    document.getElementById("textoIglesia").style.visibility = "visible";
    
    document.getElementById("parque").style.visibility = "hidden";
    document.getElementById("textoParque").style.visibility = "hidden";

    document.getElementById("parqueGranjas").style.visibility = "hidden";
    document.getElementById("textoParquegranjas").style.visibility = "hidden";
    
    document.getElementById("salon").style.visibility = "hidden";
    document.getElementById("textoSalon").style.visibility = "hidden";
    

  }

  function clickParque(e){
    console.log('parque');

    document.getElementById("iglesia").style.visibility = "hidden";
    document.getElementById("textoIglesia").style.visibility = "hidden";
    
    document.getElementById("parque").style.visibility = "visible";
    document.getElementById("textoParque").style.visibility = "visible";

    document.getElementById("parqueGranjas").style.visibility = "hidden";
    document.getElementById("textoParquegranjas").style.visibility = "hidden";

    document.getElementById("salon").style.visibility = "hidden";
    document.getElementById("textoSalon").style.visibility = "hidden";
    

  }

  function clickParquegranjas(e){
    console.log('parqueGranjas');

    document.getElementById("iglesia").style.visibility = "hidden";
    document.getElementById("textoIglesia").style.visibility = "hidden";
    
    document.getElementById("parque").style.visibility = "hidden";
    document.getElementById("textoParque").style.visibility = "hidden";

    document.getElementById("parqueGranjas").style.visibility = "visible";
    document.getElementById("textoParquegranjas").style.visibility = "visible";

    document.getElementById("salon").style.visibility = "hidden";
    document.getElementById("textoSalon").style.visibility = "hidden";
    

  }


  function clickSalon(e){
    console.log('salon');

    document.getElementById("iglesia").style.visibility = "hidden";
    document.getElementById("textoIglesia").style.visibility = "hidden";
    
    document.getElementById("parque").style.visibility = "hidden";
    document.getElementById("textoParque").style.visibility = "hidden";

    document.getElementById("parqueGranjas").style.visibility = "hidden";
    document.getElementById("textoParquegranjas").style.visibility = "hidden";

    document.getElementById("salon").style.visibility = "visible";
    document.getElementById("textoSalon").style.visibility = "visible";
    

  }


  marker.on=('click', clickIglesia);
  marker1.on=('click',clickSalon);
  marker2.on=('click', clickParquegranjas);
  marker3.on=('click', clickParque);
  
}

var miBoton = document.getElementById("boton");

// When the user scrolls down 20px from the top of the document, show the button
window.onscroll = function() {scrollFunction()};

function scrollFunction() {
  if (document.body.scrollTop > 30000 || document.documentElement.scrollTop > 30000) {
    miBoton.style.display = "block";
  } else {
    miBoton = "none";
  }
}
// When the user clicks on the button, scroll to the top of the document
function topFunction() {
  document.body.scrollTop = 0;
  document.documentElement.scrollTop = 0;
}