function graficarMapa(){
    var mymap = L.map('mapid').setView([4.570810232930141, -74.11494970321657], 17);
    let mosaico = L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
        maxZoom: 18,
        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' + '<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' + 'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
        id: 'mapbox/streets-v11',
        tileSize: 512,
        zoomOffset: -1   
   
    }).addTo(mymap);

    var polygon = L.polygon([          
        [
          4.571869008848348,
          -74.11486387252808
          
        ],
        [
          4.571778103906664,
          -74.11527156829834
        
        ],
        [
          4.5716604622003,
          -74.11589920520782
          
        ],
        [
          4.5716016413398854,
          -74.11615133285521
          
        ],
        [
          4.570879748568707,
          -74.11600649356842
          
        ],
        [
          4.5704626546361435,
          -74.11589920520782
          
        ],
        [
          4.570248760217601,
          -74.11588847637177
          
        ],
        [
          4.569943960560884,
          -74.1155344247818
          
        ],
        [
          4.569714023891993,
          -74.11576509475708
          
        ],
        [
          4.5695910344806805,
          -74.11530375480652
          
        ],
        [
          4.569858402739206,
          -74.11496043205261
          
        ],
        [
          4.569949307924407,
          -74.11431133747101
          
        ],
        [
          4.570361054795294,
          -74.11324381828308
          
        ],
        [
          4.571366357849909,
          -74.1140967607498
          
        ],
        [
          4.571885050895688,
          -74.11415040493011
          
        ],
        [
          4.572002692565169,
          -74.11435961723326
          
        ],
        [
          4.571869008848348,
          -74.11486387252808
          
        ]],{
          
  color: 'black',
  fillColor: '#E14DE8',
  fillOpacity: 0.3,
  
  }).addTo(mymap);



   

    var popup = L.popup();
    function onMapClick(e) {
        console.log(e.latlng.toString());
        document.getElementById("latitud").value = e.latlng.lat;
        document.getElementById("longitud").value = e.latlng.lng;
        

	    popup
			.setLatLng(e.latlng)
			.setContent("Esto es Granjas de San Pablo " + e.latlng.toString())
            .openOn(mymap);
	}
	mymap.on('click', onMapClick);




}