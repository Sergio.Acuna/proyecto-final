function formulario(){
  let eventnameValue =document.getElementById("nombreEvento").value;
	let latitudeValue = document.getElementById("latitud").value;
    let longitudeValue = document.getElementById("longitud").value;

    let d = new Date();
    let timestampValue = d.getTime();
    
    
    var event = { eventName: eventnameValue, latitude: latitudeValue, longitude: longitudeValue ,timestamp: timestampValue};


    window.localStorage.setItem(eventnameValue, JSON.stringify(event));

}
function lista(){
  let listadesitios = document.getElementById("lista");
  let arreglo = []
  
  for(let i = 0; i < localStorage.length; i++){
  let item = localStorage.getItem(localStorage.key(i));
  let eventos = JSON.parse(item);
  arreglo.push(eventos);

  }

  let orden = ordenar(arreglo);

  for(let i = 0;i < orden.length; i++){
    console.log(orden[i].evento);
    let elemento = document.createElement("li");
    let texto = document.createTextNode(orden[i].evento);
    elemento.appendChild(texto);
    listadesitios.appendChild(elemento);
    
}
}

function ordenar(myArray){
  let i;
  let temporal;
  let j; 
  for (i = 0; i < myArray.length  ; i++){
    for (j = 1; j  < (myArray.length - i) ; j++){
        if(myArray[j-1].timestamp > myArray[j].timestamp){
          temporal = myArray[j-1];
          myArray[j-1] = myArray[j];
          myArray[j] = temporal;
        }
    }
  }
  return myArray;
}
